import { Link } from 'gatsby';
import React from 'react';
import SEO from '../components/seo';
import TransformOembedToIframe from '../utils/TransformOembedToIframe';

interface Props {
  pathContext: {
    data: any;
  };
}

const Blog: React.FC<Props> = ({ pathContext: { data } }) => {
  return (
    <>
      <SEO
        title={data.seo_title}
        description={data.seo_description}
        img={data.miniature.url}
      />
      <Link to="/">Regresar</Link>
      <h1>{data.title}</h1>
      <div className="markdown-body">
        <div
          dangerouslySetInnerHTML={{
            __html: TransformOembedToIframe(data.content),
          }}
        ></div>
      </div>
    </>
  );
};

export default Blog;
