import React from 'react';
import { graphql, Link } from 'gatsby';
import { URISTRAPI } from '../config/strapi';

export default function Home({
  data: {
    allStrapiPost: { nodes },
  },
}) {
  console.log('props', nodes);
  return (
    <div>
      <h1>Home del blog</h1>
      {nodes?.length &&
        nodes.map((post) => {
          console.log(post.miniature[0]);
          return (
            <Link key={post.id} to={`/blog/${post.url}`}>
              <img src={`${URISTRAPI}${post.miniature[0].url}`} />
              {post.title}
            </Link>
          );
        })}
    </div>
  );
}

export const query = graphql`
  query {
    allStrapiPost(
      sort: { fields: createdAt, order: DESC }
      filter: { type: { eq: "experience" } }
    ) {
      nodes {
        id
        seo_title
        seo_description
        miniature {
          url
        }
        content
        title
        url
      }
    }
  }
`;
