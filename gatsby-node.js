/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path');

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;

  const posts = await graphql(`
    query {
      allStrapiPost(sort: { fields: createdAt, order: DESC }) {
        nodes {
          id
          seo_title
          seo_description
          miniature {
            url
          }
          content
          title
          url
        }
      }
    }
  `);

  console.log('posts', posts.data.allStrapiPost.nodes);

  posts.data.allStrapiPost.nodes.forEach((post) => {
    createPage({
      path: `/blog/${post.url}`,
      component: path.resolve(`src/templates/blog.tsx`),
      context: {
        data: post,
      },
    });
  });
};
